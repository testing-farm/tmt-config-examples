# tmt config examples

This repository holds a proposal for simple, config-parser like  configuration from tmt.

The data is located in (starting with least privileged, going to most privileged):

* /etc directory
* .config/tmt home subdirectory
* .tmt local directory
